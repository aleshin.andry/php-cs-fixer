<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude([
        'vendor',
        'resources/views'
    ])
    ->in(__DIR__);

$config = new PhpCsFixer\Config();

return $config->setRules([
    '@Symfony' => true,
    '@PSR12'   => true,

    'array_indentation' => true,

    'binary_operator_spaces' => [
        'default' => 'align_single_space_minimal',
    ],

    'align_multiline_comment' => [
        'comment_type' => 'phpdocs_like',
    ],

    'multiline_comment_opening_closing' => true,
    'method_chaining_indentation'       => true,
    'no_null_property_initialization'   => true,
    'no_useless_return'                 => true,
    'operator_linebreak'                => true,
    'ordered_class_elements'            => [
        'order' => [
            'use_trait',
            'case',
            'constant_public',
            'constant_protected',
            'constant_private',
            'property_public',
            'property_public_static',
            'property_protected',
            'property_protected_static',
            'property_private',
            'property_private_static',
            'construct',
            'destruct',
            'magic',
            'phpunit',
            'method_public',
            'method_public_static',
            'method_protected',
            'method_protected_static',
            'method_private',
            'method_private_static',
        ],
    ],

    'phpdoc_order'               => true,
    'no_superfluous_phpdoc_tags' => false,

    'phpdoc_types_order' => [
        'sort_algorithm'  => 'alpha',
        'null_adjustment' => 'always_last',
    ],

    'phpdoc_add_missing_param_annotation' => [
        'only_untyped' => false,
    ],

    'blank_line_before_statement' => [
        'statements' => [
            'phpdoc',
            'if',
            'foreach',
            'break',
            'continue',
            'declare',
            'return',
            'throw',
            'try',
            'exit',
        ],
    ],

    'no_extra_blank_lines' => [
        'tokens' => [
            'break',
            'case',
            'continue',
            'curly_brace_block',
            'default',
            'extra',
            'parenthesis_brace_block',
            'return',
            'square_brace_block',
            'switch',
            'throw',
            'use',
            'use_trait',
        ],
    ],

    'single_line_comment_style'   => true,
    'single_import_per_statement' => false,
    'group_import'                => true,
    'ordered_traits'              => true,

    'class_attributes_separation' => [
        'elements' => [
            'trait_import' => 'only_if_meta',
            'const'        => 'only_if_meta',
            'property'     => 'none',
            'case'         => 'one',
            'method'       => 'one',
        ],
    ],
])->setFinder($finder);
