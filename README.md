## О проекте
Это кастомная конфигурация [PHP-CS-Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer).

## Предварительная настройка
Для запуска необходимо скачать [**php-cs-fixer.phar**](https://github.com/FriendsOfPHP/PHP-CS-Fixer/releases).

## Использование

Запуск исправления файлов.
```
./php-cs-fixer.phar fix --config=./.php-cs-fixer.dist.php --allow-risky=yes ./
```

Запуск исправления файлов с отображением изменений.
```
./php-cs-fixer.phar fix --config=./.php-cs-fixer.dist.php --allow-risky=yes --diff ./
```

Показать все исправления которые могут быть применены (без фиксации изменений).
```
./php-cs-fixer.phar fix --config=./.php-cs-fixer.dist.php --allow-risky=yes --dry-run --diff ./
```